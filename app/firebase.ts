"use server";
// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";
import { getFirestore, collection, doc, getDocs, query, getDoc } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration

const firebaseConfig = {
  apiKey: "AIzaSyAm9zZ_RMlhg5CRv01NAROY52RnJDhpFYU",
  authDomain: "peterpark-challenge.firebaseapp.com",
  projectId: "peterpark-challenge",
  storageBucket: "peterpark-challenge.appspot.com",
  messagingSenderId: "498847104220",
  appId: "1:498847104220:web:cdad09d34dd0f34db8c819"
};

// Initialize Firebase

const app = initializeApp(firebaseConfig);
const store = getFirestore(app);
const messagesRef = collection(store, "messages");
const docRef = doc(messagesRef, "25aTNKeCpYc7HuRQ9iXD");

async function getMessage() {
  const q = query(messagesRef);
  // const snap = await getDocs(q);
  // if (snap.docs.length > 0) {
  //   return "ok";
  // }
  // return "ko";
  const snap = await getDoc(docRef);
  if (snap.exists()){
    return snap.data();
  }
  return {};
}

export {
  getMessage
}
