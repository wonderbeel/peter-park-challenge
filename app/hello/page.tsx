import { getMessage } from "../firebase";

export default async function Page() {
  const message = "Hello World";
  const remote = await getMessage();

  return (
    <main>
    <div>LOCAL: {message}</div>
    <div>REMOTE: {remote.message}</div>
    </main>
  );
}
